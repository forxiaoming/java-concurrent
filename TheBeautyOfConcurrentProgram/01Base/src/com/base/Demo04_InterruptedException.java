package com.base;

/**
 * InterruptedException
 **/
public class Demo04_InterruptedException {
    public static void main(String[] args) {
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("ThreadA begin run");
                for (; ; ) {
                }
            }
        });

        // 获取当前线程 (currentThread是返回该代码段被哪个线程调用,  即这里为main线程
        final Thread currentThread = Thread.currentThread();

        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("child threadB overe");

                // 中断主线程
                currentThread.interrupt();
//                threadA.interrupt();

            }
        });

        threadA.setName("threadA");
        threadA.start();

        threadB.start();


        try {
            threadA.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
