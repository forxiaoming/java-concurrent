package com.base;

public class Demo07_IsInterruptedAndInterrupted {

    public static void main(String[] args) throws InterruptedException {
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0 ; i < 10000000 ; i++ ) {

                }
            }
        });

        threadA.start();
        System.out.println("isInterrupted: " + threadA.isInterrupted());
        // 设置中断标志
        threadA.interrupt();
        // 给main thread 也设置中断标志
        Thread.currentThread().interrupt();

        // 获取中断标志
        System.out.println("isInterrupted: " + threadA.isInterrupted());

        System.out.println("main thread isInterrupted: " + Thread.currentThread().isInterrupted());

        // 获取中断标志并重置 (  这里中断重置的当前线程, 即main thread
        System.out.println("isInterrupted: " + threadA.interrupted());
        //或System.out.println("isInterrupted: " + Thread.interrupted());

        System.out.println("main thread isInterrupted: " + Thread.currentThread().isInterrupted());

        // 获取中断标志
        System.out.println("isInterrupted: " + threadA.isInterrupted());

        threadA.join();

        System.out.println("main thread is over");
    }

}
