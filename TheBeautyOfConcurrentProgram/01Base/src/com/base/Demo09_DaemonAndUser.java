package com.base;

public class Demo09_DaemonAndUser {
    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (; ; ) {

                }
            }
        });
        // 设置为守护进程
        // 未设置未守护进程之前, jvm 不会停止
        // 设置后jvm停止
        thread.setDaemon(true);

        thread.start();
        System.out.close();

        System.out.println("main thread is over");
    }
}
