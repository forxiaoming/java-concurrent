package com.base;

/**
 * 通过资源申请的有序性避免死锁
 */
public class Demo08_DeadLock_How {
    // 创建资源
    private static Object resourceA = new Object();
    private static Object resourceB = new Object();

    public static void main(String[] args) {
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (resourceA) {
                    System.out.println("threadA get ResourceA");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("threadA waiting get ResourceB...");
                    synchronized (resourceB) {
                        System.out.println("threadA get ResourceB");
                    }

                }
            }
        });

        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {

                // 在改变申请资源的顺序 (在Demo08_DeadLock_Why.Java 的基础上)

                synchronized (resourceA) {
                    System.out.println("threadB get ResourceA");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("threadB waiting get ResoruceB..");

                    synchronized (resourceB) {
                        System.out.println("threadB get resourceB");
                    }

                }
            }
        });

        threadA.start();
        threadB.start();

    }

}
