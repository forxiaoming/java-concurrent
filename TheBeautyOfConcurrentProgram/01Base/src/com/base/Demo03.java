package com.base;

/**
 * 2. notify()函数
 **/
public class Demo03 {

    private static volatile Object resourceA = new Object();

    public static void main(String[] args) throws InterruptedException {

        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    // 获取resourceA共享资源的监视器锁
                    synchronized (resourceA) {
                        System.out.println("ThreadA get resourceA lock");

                        System.out.println("ThreadA begin wait");
                        resourceA.wait();
                        System.out.println("ThreadA end wait");

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });


        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // 获取监视器锁
                    synchronized (resourceA) {
                        System.out.println("ThreadB get resourceA lock");

                        System.out.println("ThreadB begin wait");
                        // 阻塞线程
                        resourceA.wait();
                        System.out.println("ThreadB end wait");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });


        Thread threadC = new Thread(new Runnable() {

            @Override
            public void run() {
                synchronized (resourceA) {
                    System.out.println("ThreadC begin nofity");
                    //
//                    resourceA.notify();
                    resourceA.notifyAll();
                }
            }
        });

        threadA.start();
        threadB.start();

        Thread.sleep(1000);
        threadC.start();

        // 等待线程结束
        threadA.join();
        threadB.join();
        threadC.join();

        System.out.println("main over");
        System.exit(0);

    }
}
