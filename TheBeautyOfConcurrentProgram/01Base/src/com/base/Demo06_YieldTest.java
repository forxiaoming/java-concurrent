package com.base;

/**
 * 让出CPU执行权.
 *
 **/
public class Demo06_YieldTest implements Runnable {
    Demo06_YieldTest() {
        // 创建并启动线程
        Thread t =new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            //当i=0时让出CPU执行权, 放弃时间片, 进行下一轮调度
            //System.out.println(Thread.currentThread() + ": " + i);
            if ((i % 5) == 0) {
                System.out.println(Thread.currentThread() + "yield cpu ...");

                // 当前线程让出CPU...
                Thread.yield();
            }

        }
        System.out.println(Thread.currentThread() + " is over");
    }

    public static void main(String[] args) {
        new Demo06_YieldTest();
        new Demo06_YieldTest();
        new Demo06_YieldTest();
    }
}
