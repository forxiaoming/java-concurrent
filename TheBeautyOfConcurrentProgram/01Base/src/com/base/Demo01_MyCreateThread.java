package com.base;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 创建 Thread
 **/
public class Demo01_MyCreateThread {
    public static void main(String[] args) {
        // 使用继承: 方便传参
        MyExtendsThread extendsThread = new MyExtendsThread("ming");
        extendsThread.start();

        extendsThread.setName("继承线程");

        RunableTask runableTask = new RunableTask();
        new Thread(runableTask).start();
        new Thread(runableTask).start();

        //  创建异步任务
        FutureTask<String> futureTask = new FutureTask<>(new CallerTask());
        // 启动线程
        new Thread(futureTask).start();

        try {
            // 等待任务结束后返回结果
            String result = futureTask.get();
            System.out.println(result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }
}

class MyExtendsThread extends Thread {
    private String MyName;

    public String getMyName() {
        return MyName;
    }

    public void setMyName(String myName) {
        MyName = myName;
    }

    public MyExtendsThread(String myName) {
        this.setMyName(myName);
    }

    @Override
    public void run() {
        System.out.println("I'm a Thread by extends Thread; Hello , " + this.getMyName());
        System.out.println("Name: " + this.getName() );
        System.out.println("id: " +  this.getId());
        System.out.println("Priority: " + this.getPriority());
        System.out.println("State: " + this.getState());
    }
}

class RunableTask implements Runnable {

    @Override
    public void run() {
        System.out.println("I'm a Thread by implements Runnable");
    }
}

class CallerTask implements Callable<String> {

    @Override
    public String call() throws Exception {

        return "I'm a Thread by implements Callable";
    }
}

