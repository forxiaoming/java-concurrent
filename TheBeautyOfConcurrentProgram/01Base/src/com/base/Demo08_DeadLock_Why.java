package com.base;

public class Demo08_DeadLock_Why {
    // 创建资源
    private static Object resourceA = new Object();
    private static Object resourceB = new Object();

    public static void main(String[] args) {
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (resourceA) {
                    System.out.println("threadA get ResourceA");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("threadA waiting get ResourceB...");
                    synchronized (resourceB) {
                        System.out.println("threadA get ResourceB");
                    }

                }
            }
        });

        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (resourceB) {
                    System.out.println("threadB get ResourceB");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("threadB waiting get ResoruceA..");

                    synchronized (resourceA) {
                        System.out.println("threadB get resourceA");
                    }

                }
            }
        });

        threadA.start();
        threadB.start();

    }

}
