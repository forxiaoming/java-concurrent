package com.base;

/**
 * sleep
 * 当一个线程处于睡眠状态时, 另一个线程中断它, 则在sleep()调用处抛出异常
 **/
public class Demo05_SleepInterruptedException {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    System.out.println("child thread is in sleep");
                    Thread.sleep(10000);
                    System.out.println("child thread is in awaked");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

        // 主线程休眠
        Thread.sleep(2000);

        // 主线程中断子线程
        thread.interrupt();
    }
}
