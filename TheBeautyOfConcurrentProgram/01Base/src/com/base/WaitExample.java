package com.base;

import javax.management.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * 简单的生产者和消费者例子
 *
 * @author xiaoming
 * @data 2019/6/1 17:43
 * @versio 1.0
 **/
public class WaitExample {
    private final static int MAX_SIZE = 2;

    public static void main(String[] args) {
        List list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);

        synchronized (list) {
            // 消费队列满, 则等待队列空闲
            while (list.size() == MAX_SIZE) {
                try {
                    // 挂起线程
                    list.wait();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                

            }
        }

    }
}

// 生产线程
class ProductionThread extends Thread {


}


// 消费者线程
