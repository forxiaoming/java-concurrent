package com.base;

public class Demo10_Instance {
    // 创建ThreadLocal变量
    static ThreadLocal<String> localVariable = new ThreadLocal<>();

    // 打印函数
    static void print(String string) {
        //打印当前线程本地内存中的变量的值
        System.out.println( string + " : " + localVariable.get() );

        // 清除当前线程本地内存中变量的值
        //localVariable.remove();
    }
    public static void main(String[] args) {

        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                // 设值
                localVariable.set("threadA localVariable");
                // 调用print()
                print("threadA");
                // 打印remove后的值
                System.out.println("threadA remove after: " + localVariable.get());
            }
        });

        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                // 设值
                localVariable.set("threadB localVariable");
                // 调用print()
                print("threadB");
                // 打印remove后的值
                System.out.println("threadB remove after: " + localVariable.get());
            }
        });

        threadA.start();
        threadB.start();
    }
}
