package com.base;

public class Demo10_NoSuppotInheritance {
    // 创建线程变量
    // ThreadLocal 不支持继承
//    public static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    //  使用InheritableThreadLocal 来解决继承问题
    public static ThreadLocal<String> threadLocal = new InheritableThreadLocal<>();

    public static void main(String[] args) {
        threadLocal.set("Hello ThreadLocal");

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                // 子线程输出线程变量的值
                System.out.println("thread: " + threadLocal.get()); // null
            }
        });

        thread.start();

        // 输出主线程变量的值
        System.out.println("main Thread: " + threadLocal.get()); // Hello ThreadLocal
    }
}
