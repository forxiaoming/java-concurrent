package com.base;

/**
 * 等待线程终止 join()
 **/
public class Demo04_Join {
    public static void main(String[] args) throws InterruptedException {

        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("child threadA overe");

            }
        });

        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("child threadB overe");

            }
        });

        // 启动子线程
        threadA.start();
        threadB.start();

        System.out.println("wait all child thread over");
        // main thread will wait threadA and threadB
        threadA.join();
        threadB.join();

        System.out.println("all child thread over");

    }
}
